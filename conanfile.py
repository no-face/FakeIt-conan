from conans import ConanFile, tools
from os import path

class FakeIt(ConanFile):
    """
    FakeIt is "A simple yet very expressive, headers only library for c++ mocking.".
    See: https://github.com/eranpeer/FakeIt
    """
    name = "FakeIt"
    version = "2.0.4"
    license="MIT"
    url = "https://gitlab.com/no-face/FakeIt-conan"

    build_policy="missing" #header-only, so it is ok

    options = {
        # Header version from
        "header_version" : ["boost", "catch", "gtest", "mstest", "qtest", "standalone", "tpunit"],
    }
    default_options = "header_version=standalone"

    REPO = "https://github.com/eranpeer/FakeIt"

    EXTRACTED_FOLDER = name + "-" + version
    ZIP_URL_NAME = version + '.tar.gz'
    SHA = "4deb581b0c0aefdd90d572ae54a2325043bb27837d7bbc2fd9faf164dd12941a"

    def source(self):
        # Clone the repository
        zip_name = self.ZIP_URL_NAME
        tools.download("https://github.com/eranpeer/FakeIt/archive/%s" % self.ZIP_URL_NAME, zip_name)
        tools.check_sha256(zip_name, self.SHA)
        tools.untargz(zip_name)

    def package_id(self):
        #Ignore header_version to generate package hash (it affects only the package_info)
        self.info.options.header_version = ""

    def package(self):
        self.copy("*", path.join("include", "fakeit"), path.join(self.EXTRACTED_FOLDER, 'single_header'))

    def package_info(self):
        base_incdir = path.join("include", "fakeit")
        header_dir = path.join(base_incdir, str(self.options.header_version));

        self.cpp_info.includedirs = [base_incdir, header_dir]

