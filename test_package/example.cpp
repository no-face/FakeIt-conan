/** Example to test FakeIt package
 * Adapted from example on FakeIt README
*/

#include <iostream>
#include "fakeit.hpp"

using namespace std;
using namespace fakeit;

struct SomeInterface {
    virtual int foo(int) = 0;
    virtual int bar(string) = 0;
};

int main() {
    std::cout<<"*** Running FakeIt example ***" << std::endl;

    Mock<SomeInterface> mock;

    When(Method(mock,foo)).Return(0);

    SomeInterface &i = mock.get();

    // Production code
    i.foo(1);

    // Verify method mock.foo was invoked.
    Verify(Method(mock,foo));

    // Verify method mock.foo was invoked with specific arguments.
    Verify(Method(mock,foo).Using(1));

    return 0;
}
